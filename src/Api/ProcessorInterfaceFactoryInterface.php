<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Processor\Api;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Interface ProcessorInterfaceFactoryInterface.
 *
 * @api
 */
interface ProcessorInterfaceFactoryInterface extends FactoryInterface
{
    /**
     * {@inheritdoc}
     *
     * @return ProcessorInterface
     */
    public function create(array $arguments = []): ProcessorInterface;
}
