<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Processor\Api;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use Interactiv4\Contracts\Processor\Exception\ProcessorException;

/**
 * Interface ProcessorInterface.
 *
 * @api
 */
interface ProcessorInterface
{
    /**
     * Performs some processing, optionally using supplied data.
     *
     * @param DataObjectInterface|null $data
     *
     * @throws ProcessorException
     */
    public function process(?DataObjectInterface $data = null): void;
}
