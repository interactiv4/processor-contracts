<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Processor\Exception;

use RuntimeException;

/**
 * Class ProcessorException.
 *
 * @api
 */
class ProcessorException extends RuntimeException
{
}
